#!/bin/bash
# shbang
# Judah Tanninen

flash="/media/judahtanninen/Judah";

# Remove all files from current build
rm $flash/slp_code/current_build/*

cp /var/www/slp_code/electron/dist/*.AppImage $flash/slp_code/current_build/;
cp /var/www/slp_code/electron/dist/*.AppImage $flash/slp_code;