var { app, BrowserWindow } = require('electron');

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    show: false,
    backgroundColor: '#000000',
    autoHideMenuBar: true
  });
  const loadingWindow = new BrowserWindow({
    width: 800,
    height: 600,
    show: false,
    backgroundColor: '#000000',
    autoHideMenuBar: true
  });
  // Load both files
  loadingWindow.loadFile('./html/loadingScreen.html');
  mainWindow.loadFile('./html/index.html');

  // Once loading window finishs, set little 2 second timer to simulate loads lol
  loadingWindow.once('ready-to-show', loadingWindow.show);
  mainWindow.once('ready-to-show', () => {
    setTimeout(() => {
      loadingWindow.close();
      mainWindow.show();
      mainWindow.maximize()
    }, 1000)
  });
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
